#

# intent:
#  capture an idea and stamp it!
export IPFS_PATH=$HOME/.../ipfs/usb/FILED

ip=$(curl -s https://postman-echo.com/ip?format=text)

if [ ! -e ideas.lof ]; then
locate -b ideas.md > ideas.lof
fi


echo "--- # $0" > qm.log
cat ideas.lof | while read f; do
ticns=$(date +%s%N)
qm=$(ipfs add "$f" -Q)
mtime=$(stat -c %Y "$f")
temp=$(sensors -u | grep 'temp.*_in' | sort -n +1 | tail -1 | cut -c16-20)
echo $mtime: $qm $ip $temp $f >> qm.log
done
sort +1n qm.log | dee qm.log
