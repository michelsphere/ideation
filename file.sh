
export IPFS_PATH=$HOME/.../ipfs/usb/FILED

ip=$(curl -s https://postman-echo.com/ip?format=text)

file=${1:-an-idea.md}
date=$(date +%Y-%m-%d)
ticns=$(date +%s%N)
temp=$(sensors -u | grep 'temp.*_in' | sort -n +1 | tail -1 | cut -c16-20)

mtime=$(stat -c %Y "$file")
qm=$(ipfs add "$file" -Q)
ots stamp "$file"

echo $ticns $qm $ip $temp
echo $mtime: $qm $ip $temp $file >> qm.log
